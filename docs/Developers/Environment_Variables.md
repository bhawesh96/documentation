# GitMate Environment Variables

GitMate requires you to set some environment variables.
This page provides a description of these Environment Variables and also contain a guide on how
to set them.

To set an environment variable in bash run
```bash
$ export ENVIRONMENT_VAR=VALUE
```

To check if an environment variable has been set use
```bash
$ echo $ENVIRONMENT_VAR
```
This command will print the current value of the variable.

The best way to set environment variables for this project is to use a script. See our [guide](./Bash_Scripts)

## Environment Variables

| Variable | Description | Values | Default |
|----------|-------------|---------|--------|
| DJANGO_DEBUG | Run django in debug mode | True or False | False
| DJANGO_SECRET_KEY | Cryptographic signing for django. For more details see[Django documentation](https://docs.djangoproject.com/en/1.11/ref/settings/#secret-key). | Anything complex string you can come up with. | Django genereated key
| DJANGO_ALLOWED_HOSTS | List of allowed hosts. Required to disable CORS. | Allowed host URLs | 127.0.0.1 for development setup
| DB_NAME | Database name for PostgreSQL. | Database name used by gitmate. |`postgres` |
| DB_USER | Database username for PostgreSQL. | Username String | `postgres` |
| DB_PASSWORD | Database password for PostgreSQL. | Password String | An empty string |
| DB_ADDRESS | Database host address for PostgreSQL. | String | A Auto-configured via socket on `/run/postgresql` |
| DB_PORT | Database server post for PostgreSQL | Number | Auto-configured to port `5432` |
| DJANGO_STATIC_ROOT | Directory for storing django static files. | File path | `/tmp/static` |
| CELERY_BROKER_URL | URI fro celery broker | URI path | `amqp://admin:password@rabbit/` |
| SOCIAL_AUTH_LOGIN_REDIRECT_URL | URL to redirect after authentication | Valid URLs | Frontend Home `localhost:4200` |
| SOCIAL_AUTH_GITHUB_KEY | Client key for the registered Github OAuth Application. This will be used for application authentication on Github. | Key provided by Github on registration | None |
| SOCIAL_AUTH_GITHUB_SECRET | Client secret for the registered Github OAuth Application. This is used as application password by Github | Secret provided by Github | None |
| SOCIAL_AUTH_GITLAB_KEY | Client key for the registered Gitlab OAuth Application. This will be used for application authentication on Gitlab. | Key provided by Gitlab on registration | None |
| SOCIAL_AUTH_GITLAB_SECRET | Client secret for the registered Gitlab OAuth Application. This is used as application password by Gitlab. | Secret provided by Gitlab. | None |
| SOCIAL_AUTH_GITLAB_REDIRECT_URL | URL to redirect after Gitlab authentication. | Same as the `Callback URL` in registered Gitlab OAuth Applicaition. | None |
| WEBHOOK_SECRET | A secret key to register git hosting provider webhooks with. Improves security | Any string you can come up with | None |
| GITHUB_APP_PRIVATE_KEY | Private key is used to authorize Github. This will allow usage of Github apps.| Path to Github private key | Random private key |

!!! note
    - [How to register Github OAuth App?](https://developer.github.com/apps/building-integrations/setting-up-and-registering-oauth-apps/registering-oauth-apps/)
    - [How to register Gitlab OAuth App?](https://docs.gitlab.com/ee/integration/oauth_provider.html#adding-an-application-through-the-profile)

    Provide `https://<yourdomain>:<port>/auth/complete/<hoster>` as `Authorization callback URL`.
