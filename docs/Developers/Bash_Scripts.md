# Shell Scripts

GitMate requires certain [environment variables](./Environment_Variables)
to be set. The easiest method to do this is to source a script.

Shell scripts are a series of shell commands executed sequentially. Here we will walk through how to set up and execute a script to set up all the required variables.

First we will create our script
(We recommend placing your script outside your cloned repo).

```bash
$ touch my_script.sh
```

Now open ```my_script.sh``` in your favorite text editor.

The first element in our script is as follows
```bash
#!/bin/bash
```
This tells our shell to use a bash interpreter when running our script.

After that, set the environment variables using the export command.
```bash
export $VARIABLE=Value
```

Finally, in order to run our script use
```bash
$ source my_script.sh
```
